﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Rotator : MonoBehaviour
{
    [SerializeField] private float deltaAngle;

    // Use this for initialization
    void Start()
    {

    }

    // Update is called once per frame
    void FixedUpdate()
    {
        var xRotation = Quaternion.AngleAxis(deltaAngle, Vector3.up);
        transform.rotation *= xRotation;
    }
}
